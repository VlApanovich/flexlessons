package com.flashlesson.components
{
	
	import com.flashlesson.skins.BeeperSkin;
	
	import flash.events.MouseEvent;
	import flash.events.TimerEvent;
	import flash.media.Sound;
	import flash.utils.Timer;
	import flash.utils.getTimer;
	
	import mx.controls.Alert;
	
	import spark.components.Button;
	import spark.components.supportClasses.SkinnableComponent;
	
	
	
	public class BeeperComponent extends SkinnableComponent
	{
		private var timerAtStart:int = 0;	
		private static const DELAY:uint = 1000;
		private var beepTimer:Timer;
		
		[Embed('assets/test.mp3')]
		private var soundClass:Class;
		
		
		private var d:Date;		
		[Bindable] public var sec:int = 0;
		[Bindable] public var min:int = 0;
		
		[Bindable]
		public var inputField:String;
		
		[SkinPart(required="true")]
		public var startButton:Button;
		[SkinPart(required="true")]
		public var stopButton:Button;
		
		
		public function BeeperComponent()
		{
			super();
			setStyle("skinClass", BeeperSkin );
		}
		
		override protected function getCurrentSkinState():String
		{
			return super.getCurrentSkinState();
		} 
		
		override protected function partAdded(partName:String, instance:Object) : void
		{
			super.partAdded(partName, instance);
			if (instance == startButton)
			{
				startButton.addEventListener(MouseEvent.CLICK, startButton_Clicked);
			}
			if (instance == stopButton)
			{
				stopButton.addEventListener(MouseEvent.CLICK, stopButton_Clicked);
			}
		}
		
		override protected function partRemoved(partName:String, instance:Object) : void
		{
			super.partRemoved(partName, instance);
			if (instance == startButton)
			{
				startButton.removeEventListener(MouseEvent.CLICK, startButton_Clicked);
			}
			if (instance == stopButton)
			{
				stopButton.removeEventListener(MouseEvent.CLICK, stopButton_Clicked);
			}
		}
		
		public function startButton_Clicked(event:MouseEvent):void
		{
			if(!inputField){
				Alert.show("Please fix your Timer interval.");
			}
			else{
			startBeepTimer(parseInt(inputField));
			}
		}
		
		public function stopButton_Clicked(event:MouseEvent):void
		{
			
			stopBeepTimer();
		}
		
		private function startBeepTimer(repeat:uint):void
		{
			
			beepTimer=new Timer(DELAY, repeat);				
			beepTimer.addEventListener(TimerEvent.TIMER, timerHandler);
			beepTimer.addEventListener(TimerEvent.TIMER_COMPLETE, completeHandler);
			timerAtStart = getTimer();
			beepTimer.start();
			startButton.enabled=false;
			//Alert.show(input1);
		}
		
		private function stopBeepTimer():void
		{
			beepTimer.reset();
			min = 0;
			sec = 0;
			startButton.enabled=true;
		}
		
		private function timerHandler(e:TimerEvent):void {
			d = new Date( getTimer() - timerAtStart );
			min = d.minutes;
			sec = d.seconds;
			
			
		}
		
		private function completeHandler(e:TimerEvent):void {
			
			startBeepTimer(parseInt(inputField));
			
			var smallSound:Sound = new soundClass() as Sound;
			smallSound.play();
			
			
		}
	}
}